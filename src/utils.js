import * as request from 'superagent';

const apiCall = (data, type, url) => {
    
    return new Promise((resolve, reject) => {
      request[type](`http://localhost:8000/api/${url}`)
        .send(data)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (res) {
            return resolve(res.body);
          }
          return reject(err);
        });
    });
  }

  export { apiCall };