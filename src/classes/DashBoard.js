
import React, { useState } from 'react';
import classnames from 'classnames';
import {
  TabContent,
  TabPane,
  Nav, 
  NavItem,
  NavLink,
  Row,
  Col
} from 'reactstrap';
import Teachers from './Teachers';
import Classes from './Classes';
import Students from './Students';

const DashBoard = () => {
  const [activeTab, setActiveTab] = useState('teachers');

  const toggle = (tab) => {
    if (activeTab !== tab){
      setActiveTab(tab)
    }
  }
  return (
    <>
      <h4 className='text-center'>Manage Learners</h4>
      <div className='row'>
        <div className='col-md-2'>&nbsp;</div>
        <div className='col-md-8'>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({
                  active:
                    activeTab === 'teachers'
                })}
                onClick={() => { toggle('teachers'); }}
              >
                Teachers
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({
                  active:
                    activeTab === 'classes'
                })}
                onClick={() => { toggle('classes'); }}
              >
                Classes
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({
                  active:
                    activeTab === 'students'
                })}
                onClick={() => { toggle('students'); }}
              >
                Students
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={activeTab}>
            <TabPane tabId="teachers">
              <Row>
                <Col sm="12">
                  <Teachers/>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="classes">
              <Row>
                <Col sm="12">
                  <Classes/>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="students">
              <Row>
                <Col sm="12">
                  <Students/>
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      </div>
    </>
  );
}

export default DashBoard;