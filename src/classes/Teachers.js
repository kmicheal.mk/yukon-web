
import React, { useEffect, useState } from 'react';
import {
    Button,
    Row,
    Col,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label,
    Form,
    FormGroup,
    FormFeedback
} from 'reactstrap';
import { apiCall } from '../utils';

const Teachers = () => {
    const [errors, setErrors] = useState({});
    const [modal, setModal] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [objectDetails, setObjectDetails] = useState({});
    const [teachers, setTeachers] = useState([]);
    const fetchTeachers = () => {
        apiCall({}, 'get', `teachers/`).then((response) => {
            setTeachers(response.results);
        }).catch(() => {
            console.log('error fetching data');
        });
    };

    const toggle = () => {
        setModal(!modal);
    };

    useEffect(() => {
        fetchTeachers();
    }, []);

    const validate = () => {
        let valid = true;
        const errorDetails = {};
        setErrors({});

        if (!objectDetails.firstname) {
            errorDetails.firstname = 'First name is required';
            valid = false;
        }

        if (!objectDetails.surname) {
            errorDetails.surname = 'Surname is required.';
            valid = false;
        }

        if (!objectDetails.id_number) {
            errorDetails.id_number = 'Staff No. is required.';
            valid = false;
        }

        setErrors(errorDetails);
        return valid;
    };

    const getInputValue = (e) => {
        setErrors({});

        // eslint-disable-next-line
        const value = e.target.value;
        const details = objectDetails;
        if (value.length > 0) {
            details[e.target.id] = value;
        } else {
            delete details[e.target.id];
        }

        setObjectDetails(details);
    };

    const saveStaff = () => {
        if (validate()) {
            let method = 'post';
            let url = 'teachers/';
            if(editMode){
                method = 'put'
                url = `${url}${objectDetails.id}/`
            }
            apiCall(objectDetails, method, url).then(() => {
                fetchTeachers();
                toggle();
            }).catch((error) => error);
        }
    };

    const openAddStaff = () =>{
        setEditMode(false);
        toggle();
    };
    const openEditForm = (teacher) =>{
        setEditMode(true);
        toggle();
        setObjectDetails(teacher)
    };

    return (
        <>
            <Row>
                <Col md='12' className='text-right mt-3'>
                    <Button
                        color="primary"
                        onClick={openAddStaff}
                    >
                        Add Teacher
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col md='12'>
                    <Table striped>
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    First Name
                                </th>
                                <th>
                                    Sur Name
                                </th>
                                <th>
                                    Staff No.
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                teachers.map((teacher, count) => (
                                    <tr key={teacher.id}>
                                        <th scope="row">
                                            {count + 1}
                                        </th>
                                        <td>
                                            {teacher.firstname}
                                        </td>
                                        <td>
                                            {teacher.surname}
                                        </td>
                                        <td>
                                            {teacher.id_number}
                                        </td>
                                        <td>
                                            <Button
                                                color="primary"
                                                outline
                                                size='sm'
                                                onClick={() => openEditForm(teacher)}
                                            >
                                                edit
                                            </Button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <Modal
                        isOpen={modal}
                        toggle={toggle}
                        backdrop='static'
                    >
                        <ModalHeader toggle={toggle}>Add Teacher</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for="firstname">
                                        Fisrt Name
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='firstname'
                                        value={objectDetails.firstname}
                                        invalid={(Object.keys(errors)).includes('firstname')}
                                    />
                                    {errors.firstname && (
                                        <FormFeedback>
                                            {errors.firstname}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="surname">
                                        Sur Name
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='surname'
                                        value={objectDetails.surname}
                                        invalid={(Object.keys(errors)).includes('surname')}
                                    />
                                    {errors.surname && (
                                        <FormFeedback>
                                            {errors.surname}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_number">
                                        Staff ID
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='id_number'
                                        value={objectDetails.id_number}
                                        invalid={(Object.keys(errors)).includes('id_number')}
                                    />
                                    {errors.id_number && (
                                        <FormFeedback>
                                            {errors.id_number}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={saveStaff}>
                                Submit
                            </Button>{' '}
                            <Button color="secondary" onClick={toggle}>
                                Close
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        </>
    );
}

export default Teachers;