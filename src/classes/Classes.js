
import React, { useEffect, useState } from 'react';
import {
    Button,
    Row,
    Col,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label,
    Form,
    FormGroup,
    FormFeedback
} from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from './CustomSelectInput';
import { apiCall } from '../utils';

const Classes = () => {
    const [errors, setErrors] = useState({});
    const [modal, setModal] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [objectDetails, setObjectDetails] = useState({});
    const [classList, setClasses] = useState([]);
    const [teachers, setTeachers] = useState({
        active: {
            label: `Search Teacher`,
            value: 0,
            key: 0
        },
        list: []
    });

    const fetchTeachers = () => {
        apiCall({}, 'get', `teachers/`).then((response) => {
            let list = [];
            response.results.map((teacher) =>{
                teacher['label'] = teacher.firstname + ' ' + teacher.surname;
                teacher['value'] = teacher.id;
                teacher['key'] = teacher.id;
                list.push(teacher);
            });
            setTeachers({
                active: list[0],
                list: list
            });
        }).catch(() => {
            console.log('error fetching data');
        });
    };

    const fetchClasses = () => {
        apiCall({}, 'get', `classes/`).then((response) => {
            setClasses(response.results);
        }).catch(() => {
            console.log('error fetching data');
        });
    };

    const toggle = () => {
        setModal(!modal);
    };

    useEffect(() => {
        fetchClasses();
        fetchTeachers();
    }, []);

    const validate = () => {
        let valid = true;
        const errorDetails = {};
        setErrors({});

        if (!objectDetails.class_label) {
            errorDetails.class_label = 'Class label is required';
            valid = false;
        }

        if(teachers.active.value === 0){
            errorDetails.teacher = 'Class teacher is required';
            valid = false;
        }

        setErrors(errorDetails);
        return valid;
    };

    const getInputValue = (e) => {
        setErrors({});

        // eslint-disable-next-line
        const value = e.target.value;
        const details = objectDetails;
        if (value.length > 0) {
            details[e.target.id] = value;
        } else {
            delete details[e.target.id];
        }

        setObjectDetails(details);
    };

    const saveClass = () => {
        if (validate()) {
            let method = 'post';
            let url = 'classes/';
            if (editMode) {
                method = 'put'
                url = `${url}${objectDetails.id}/`
            }
            objectDetails.class_teacher = teachers.active.value;
            apiCall(objectDetails, method, url).then(() => {
                fetchClasses();
                toggle();
            }).catch((error) => error);
        }
    };

    const openAddStaff = () => {
        setEditMode(false);
        toggle();
    };
    const openEditForm = (teacher) => {
        setEditMode(true);
        toggle();
        setObjectDetails(teacher)
    };

    return (
        <>
            <Row>
                <Col md='12' className='text-right mt-3'>
                    <Button
                        color="primary"
                        onClick={openAddStaff}
                    >
                        Add Class
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col md='12'>
                    <Table striped>
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Class Label
                                </th>
                                <th>
                                    Teacher
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                classList.map((teacherClass, count) => (
                                    <tr key={teacherClass.id}>
                                        <th scope="row">
                                            {count + 1}
                                        </th>
                                        <td>
                                            {teacherClass.class_label}
                                        </td>
                                        <td>
                                            {`${teacherClass.teacher_details.firstname} ${teacherClass.teacher_details.surname}`}
                                        </td>
                                        <td>
                                            <Button
                                                color="primary"
                                                outline
                                                size='sm'
                                                onClick={() => openEditForm(teacherClass)}
                                            >
                                                edit
                                            </Button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <Modal
                        isOpen={modal}
                        toggle={toggle}
                        backdrop='static'
                    >
                        <ModalHeader toggle={toggle}>Add Class</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for="class_label">
                                        Class Label
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='class_label'
                                        value={objectDetails.class_label}
                                        invalid={(Object.keys(errors)).includes('class_label')}
                                    />
                                    {errors.class_label && (
                                        <FormFeedback>
                                            {errors.class_label}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label className="mt-4" for="teacher">
                                        <span>Class Teacher</span>
                                    </Label>
                                    <Select
                                        components={{ Input: CustomSelectInput }}
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        name="teacher"
                                        id="teacher"
                                        options={teachers.list}
                                        value={teachers.active}
                                        onChange={(value) => setTeachers({...teachers, active:value})}
                                    />
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={saveClass}>
                                Submit
                            </Button>{' '}
                            <Button color="secondary" onClick={toggle}>
                                Close
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        </>
    );
}

export default Classes;