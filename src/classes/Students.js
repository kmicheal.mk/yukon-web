
import React, { useEffect, useState } from 'react';
import {
    Button,
    Row,
    Col,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label,
    Form,
    FormGroup,
    FormFeedback
} from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from './CustomSelectInput';
import { apiCall } from '../utils';

const Students = () => {
    const [errors, setErrors] = useState({});
    const [modal, setModal] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [objectDetails, setObjectDetails] = useState({});
    const [students, setStudents] = useState([]);
    const [classes, setClasses] = useState({
        active: {
            label: `Search Class`,
            value: 0,
            key: 0
        },
        list: []
    });

    const fetchClasses = () => {
        apiCall({}, 'get', `classes/`).then((response) => {
            let list = [];
            response.results.map((teacherClass) => {
                teacherClass['label'] = `${teacherClass.class_label} (${teacherClass.teacher_details.firstname} ${teacherClass.teacher_details.surname})`;
                teacherClass['value'] = teacherClass.id;
                teacherClass['key'] = teacherClass.id;
                list.push(teacherClass);
            });
            setClasses({
                active: list[0],
                list: list
            });
        }).catch(() => {
            console.log('error fetching data');
        });
    };

    const fetchStudents = () => {
        apiCall({}, 'get', `students/?class=${classes.active.value}`).then((response) => {
            setStudents(response.results);
        }).catch(() => {
            console.log('error fetching data');
        });
    };

    const toggle = () => {
        setModal(!modal);
    };

    useEffect(() => {
        fetchClasses();
    }, []);

    useEffect(() => {
        fetchStudents();
    }, [classes.active]);

    const validate = () => {
        let valid = true;
        const errorDetails = {};
        setErrors({});

        if (!objectDetails.firstname) {
            errorDetails.firstname = 'First name is required';
            valid = false;
        }

        if (!objectDetails.surname) {
            errorDetails.surname = 'Surname is required.';
            valid = false;
        }

        if (!objectDetails.student_number) {
            errorDetails.student_number = 'Student No. is required.';
            valid = false;
        }

        if (classes.active.value === 0) {
            errorDetails.class = 'Class is required';
            valid = false;
        }

        setErrors(errorDetails);
        return valid;
    };

    const getInputValue = (e) => {
        setErrors({});

        // eslint-disable-next-line
        const value = e.target.value;
        const details = objectDetails;
        if (value.length > 0) {
            details[e.target.id] = value;
        } else {
            delete details[e.target.id];
        }

        setObjectDetails(details);
    };

    const saveStudent = () => {
        if (validate()) {
            let method = 'post';
            let url = 'students/';
            if (editMode) {
                method = 'patch'
                url = `${url}${objectDetails.id}/`
            } else {
                objectDetails.student_class = classes.active.value;
            }
            apiCall(objectDetails, method, url).then(() => {
                fetchStudents();
                toggle();
            }).catch((error) => error);
        }
    };

    const openAddStaff = () => {
        setEditMode(false);
        toggle();
    };
    const openEditForm = (teacher) => {
        setEditMode(true);
        toggle();
        setObjectDetails(teacher)
    };

    return (
        <>
            <Row>
                <Col md='12'>
                    <FormGroup>
                        <Label className="mt-4" for="class">
                            <span>Class</span>
                        </Label>
                        <Select
                            components={{ Input: CustomSelectInput }}
                            className="react-select"
                            classNamePrefix="react-select"
                            name="class"
                            id="class"
                            options={classes.list}
                            value={classes.active}
                            onChange={(value) => setClasses({ ...classes, active: value })}
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col md='12' className='text-right mt-3'>
                    <Button
                        color="primary"
                        onClick={openAddStaff}
                    >
                        Add Student
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col md='12'>
                    <Table striped>
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    First Name
                                </th>
                                <th>
                                    Sur Name
                                </th>
                                <th>
                                    Student No.
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                students.map((student, count) => (
                                    <tr key={student.id}>
                                        <th scope="row">
                                            {count + 1}
                                        </th>
                                        <td>
                                            {student.firstname}
                                        </td>
                                        <td>
                                            {student.surname}
                                        </td>
                                        <td>
                                            {student.student_number}
                                        </td>
                                        <td>
                                            <Button
                                                color="primary"
                                                outline
                                                size='sm'
                                                onClick={() => openEditForm(student)}
                                            >
                                                edit
                                            </Button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <Modal
                        isOpen={modal}
                        toggle={toggle}
                        backdrop='static'
                    >
                        <ModalHeader toggle={toggle}>Add Student</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label for="firstname">
                                        Fisrt Name
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='firstname'
                                        value={objectDetails.firstname}
                                        invalid={(Object.keys(errors)).includes('firstname')}
                                    />
                                    {errors.firstname && (
                                        <FormFeedback>
                                            {errors.firstname}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="surname">
                                        Sur Name
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='surname'
                                        value={objectDetails.surname}
                                        invalid={(Object.keys(errors)).includes('surname')}
                                    />
                                    {errors.surname && (
                                        <FormFeedback>
                                            {errors.surname}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label for="student_number">
                                        Student Number
                                    </Label>
                                    <Input
                                        onInput={getInputValue}
                                        autoComplete='off'
                                        id='student_number'
                                        value={objectDetails.student_number}
                                        invalid={(Object.keys(errors)).includes('student_number')}
                                    />
                                    {errors.student_number && (
                                        <FormFeedback>
                                            {errors.student_number}
                                        </FormFeedback>
                                    )}
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={saveStudent}>
                                Submit
                            </Button>{' '}
                            <Button color="secondary" onClick={toggle}>
                                Close
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        </>
    );
}

export default Students;