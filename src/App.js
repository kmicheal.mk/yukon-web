import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import DashBoard from './classes/DashBoard';

const App = () => {
  return (
    <DashBoard/>
  );
}

export default App;
